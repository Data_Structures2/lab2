public class Lab2_3 {

    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int k = 1; // จำนวนสมาชิกที่ไม่ซ้ำกันในอาร์เรย์ใหม่

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[k] = nums[i]; // นำสมาชิกที่ไม่ซ้ำกันไปตั้งแต่ตำแหน่งที่ k
                k++;
            }
        }

        return k;
    }

    public static void main(String[] args) {
        int[] nums = {1, 1, 2, 2, 2, 3, 4, 5, 5, 6};

        System.out.print("อาร์เรย์ต้นฉบับ: ");
        printArray(nums);

        int k = removeDuplicates(nums);

        System.out.print("อาร์เรย์หลังจากลบสมาชิกที่ซ้ำกัน: ");
        for (int i = 0; i < k; i++) {
            System.out.print(nums[i] + " ");
        }
    }

    public static void printArray(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            if (i < arr.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}

