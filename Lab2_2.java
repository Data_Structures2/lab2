public class Lab2_2 {

    public static int removeElement(int[] nums, int val) {
        int left = 0;
        int right = nums.length - 1;

        while (left <= right) {
            // เลื่อนตัวชี้ left ไปทางขวาหากค่าที่ตำแหน่ง left เป็น val
            while (left <= right && nums[left] != val) {
                left++;
            }

            // เลื่อนตัวชี้ right ไปทางซ้ายหากค่าที่ตำแหน่ง right ไม่เป็น val
            while (left <= right && nums[right] == val) {
                right--;
            }

            // สลับค่าสมาชิกที่ตำแหน่ง left และ right ของอาร์เรย์
            if (left <= right) {
                int temp = nums[left];
                nums[left] = nums[right];
                nums[right] = temp;
                left++;
                right--;
            }
        }

        // คืนค่า k ซึ่งคือตำแหน่งสุดท้ายของสมาชิกในอาร์เรย์ nums ที่ไม่เท่ากับ val
        return left;
    }

    public static void main(String[] args) {
        int[] nums = {3, 2, 2, 3, 4, 5, 6};
        int val = 3;

        System.out.print("อาร์เรย์ก่อนการลบ: ");
        printArray(nums);

        int k = removeElement(nums, val);

        System.out.print("อาร์เรย์หลังการลบ: ");
        for (int i = 0; i < k; i++) {
            System.out.print(nums[i] + " ");
        }
    }

    public static void printArray(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            if (i < arr.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}
