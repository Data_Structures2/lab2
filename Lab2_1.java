public class Lab2_1 {

    // เมธอด deleteElementByIndex ลบสมาชิกในอาร์เรย์ด้วยดัชนีที่ระบุ
    public static int[] deleteElementByIndex(int[] arr, int index) {
        // ตรวจสอบว่าดัชนีที่ระบุถูกต้องหรือไม่
        if (index < 0 || index >= arr.length) {
            System.out.println("ดัชนีไม่ถูกต้อง ไม่มีการเปลี่ยนแปลงในอาร์เรย์");
            return arr;
        }

        // สร้างอาร์เรย์ใหม่ที่มีขนาดน้อยลงหนึ่งหน่วย
        int[] updatedArray = new int[arr.length - 1];
        int currentIndex = 0;

        // คัดลอกสมาชิกจากอาร์เรย์เดิมไปยังอาร์เรย์ใหม่ (ยกเว้นสมาชิกที่ต้องการลบ)
        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                updatedArray[currentIndex] = arr[i];
                currentIndex++;
            }
        }

        return updatedArray;
    }

    // เมธอด deleteElementByValue ลบสมาชิกในอาร์เรย์ที่มีค่าเท่ากับค่าที่ระบุ
    public static int[] deleteElementByValue(int[] arr, int value) {
        // ค้นหาค่าที่ต้องการลบ
        boolean found = false;
        int[] updatedArray = new int[arr.length - 1];
        int currentIndex = 0;

        // คัดลอกสมาชิกจากอาร์เรย์เดิมไปยังอาร์เรย์ใหม่ (ยกเว้นสมาชิกที่ต้องการลบครั้งแรก)
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value && !found) {
                found = true;
            } else {
                updatedArray[currentIndex] = arr[i];
                currentIndex++;
            }
        }

        // ถ้าไม่พบค่าที่ต้องการลบ คืนค่าอาร์เรย์เดิม
        if (!found) {
            System.out.println("ไม่พบค่าที่ต้องการลบในอาร์เรย์");
            return arr;
        }

        return updatedArray;
    }

    public static void main(String[] args) {
        // ทดสอบเมธอด deleteElementByIndex และ deleteElementByValue ด้วยทดสอบที่กำหนดเอง
        int[] testArray1 = {1, 2, 3, 4, 5};
        int indexToDelete1 = 2;
        int valueToDelete1 = 4;

        System.out.print("อาร์เรย์ต้นฉบับ: ");
        printArray(testArray1);

        int[] arrayAfterDeleteByIndex1 = deleteElementByIndex(testArray1, indexToDelete1);
        System.out.print("อาร์เรย์หลังจากลบสมาชิกที่ดัชนี " + indexToDelete1 + ": ");
        printArray(arrayAfterDeleteByIndex1);

        int[] arrayAfterDeleteByValue1 = deleteElementByValue(testArray1, valueToDelete1);
        System.out.print("อาร์เรย์หลังจากลบสมาชิกที่มีค่าเป็น " + valueToDelete1 + ": ");
        printArray(arrayAfterDeleteByValue1);

        // ทดสอบเมธอด deleteElementByIndex และ deleteElementByValue ด้วยทดสอบที่กำหนดเอง
        int[] testArray2 = {10, 20, 30, 40, 50};
        int indexToDelete2 = 0;
        int valueToDelete2 = 30;

        System.out.print("\nอาร์เรย์ต้นฉบับ: ");
        printArray(testArray2);

        int[] arrayAfterDeleteByIndex2 = deleteElementByIndex(testArray2, indexToDelete2);
        System.out.print("อาร์เรย์หลังจากลบสมาชิกที่ดัชนี " + indexToDelete2 + ": ");
        printArray(arrayAfterDeleteByIndex2);

        int[] arrayAfterDeleteByValue2 = deleteElementByValue(testArray2, valueToDelete2);
        System.out.print("อาร์เรย์หลังจากลบสมาชิกที่มีค่าเป็น " + valueToDelete2 + ": ");
        printArray(arrayAfterDeleteByValue2);
    }

    // เมธอดสำหรับพิมพ์อาร์เรย์
    public static void printArray(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            if (i < arr.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}
